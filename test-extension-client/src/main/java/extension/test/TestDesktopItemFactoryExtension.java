package extension.test;

import org.nuclos.api.ui.DesktopItem;
import org.nuclos.api.ui.DesktopItemFactory;
import org.nuclos.api.ui.annotation.NucletComponent;

@NucletComponent
public class TestDesktopItemFactoryExtension extends DesktopItemFactory {

	@Override
	public String getId() {
		return "TestDesktopItemFactory";
	}

	@Override
	public String getLabel() {
		return "TestDesktopItem";
	}

	@Override
	public DesktopItem newInstance() {
		return new TestDesktopItemExtension();
	}

}
