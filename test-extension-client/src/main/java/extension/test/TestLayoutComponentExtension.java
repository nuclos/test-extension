package extension.test;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Collection;
import java.util.EnumSet;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import org.nuclos.api.Preferences;
import org.nuclos.api.Property;
import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentListener;
import org.nuclos.api.ui.layout.TabController;

public class TestLayoutComponentExtension extends JPanel implements LayoutComponent {
	
	public TestLayoutComponentExtension() {
		super(new BorderLayout());
		this.add(new JLabel("TestLayoutComponent"));
	}

	@Override
	public void addLayoutComponentListener(LayoutComponentListener arg0) {
	}

	@Override
	public JComponent getComponent(LayoutComponentType arg0) {
		return this;
	}

	@Override
	public Property[] getComponentProperties() {
		return null;
	}

	@Override
	public String[] getComponentPropertyLabels() {
		return null;
	}

	@Override
	public LayoutComponentContext getContext() {
		return null;
	}

	@Override
	public Collection<LayoutComponentListener> getLayoutComponentListeners() {
		return null;
	}

	@Override
	public String getName() {
		return "TestLayoutComponent";
	}

	@Override
	public EnumSet<LayoutComponentType> getSupportedTypes() {
		return EnumSet.of(LayoutComponentType.DESIGN, LayoutComponentType.DETAIL);
	}

	@Override
	public void removeLayoutComponentListener(LayoutComponentListener arg0) {
	}

	@Override
	public void setBorder(Border arg0) {
	}

	@Override
	public void setName(String arg0) {
	}

	@Override
	public void setPreferences(Preferences arg0) {
	}

	@Override
	public void setPreferredSize(Dimension arg0) {
	}

	@Override
	public void setProperty(String arg0, Object arg1) {
	}

	@Override
	public void setTabController(TabController arg0) {
		// TODO Auto-generated method stub
		
	}

}
