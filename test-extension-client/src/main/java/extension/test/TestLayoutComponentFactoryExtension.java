package extension.test;

import javax.swing.Icon;

import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.Alignment;
import org.nuclos.api.ui.annotation.NucletComponent;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentFactory;

@NucletComponent
public class TestLayoutComponentFactoryExtension implements LayoutComponentFactory {

	@Override
	public Alignment getDefaulAlignment() {
		return null;
	}

	@Override
	public Object getDefaultPropertyValue(String arg0) {
		return null;
	}

	@Override
	public Icon getIcon() {
		return null;
	}

	@Override
	public String getName() {
		return "TestLayoutComponent";
	}

	@Override
	public LayoutComponent newInstance(LayoutComponentContext arg0) {
		return new TestLayoutComponentExtension();
	}

}
