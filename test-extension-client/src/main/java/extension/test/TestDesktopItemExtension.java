package extension.test;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.nuclos.api.ui.DesktopItem;

public class TestDesktopItemExtension extends JPanel implements DesktopItem {

	public TestDesktopItemExtension() {
		super(new BorderLayout());
		add(new JLabel("TestDesktopItem"));
	}
	
	@Override
	public JComponent getComponent() {
		return this;
	}

}
