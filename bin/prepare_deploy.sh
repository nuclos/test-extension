#!/bin/bash

# for newly created self-signed key
KEYSTORE_DNAME="cn=Firstname Lastname, ou=Unit, o=Novabit, c=DE"
KEYSTORE_KEYALIAS="extension"
KEYSTORE_STOREPASSWD="nuclos"
KEYSTORE_KEYPASSWD="nuclos"

# Do not edit beneath this line 
REGEX_DEP="^(.+):(.+):(.+):(.+):(.+)$"
MAVEN_REPOSITORY="$HOME/.m2/repository"

INSTALL_DIR=`pwd`
#INSTALL_DIR=$1
#if [ ! -d $INSTALL_DIR ]; then
#	echo "Usage: $0 <nuclos_install_dir>" 
#	exit -2
#fi
#shift

#if [ ! -d "$INSTALL_DIR/webapp/app" ]; then
#	echo "Unlikely that $INSTALL_DIR is a nuclos installation directory"
#	exit -2
#fi
if [ ! -f nuclos.txt ]; then
	echo "Missing file nuclos.txt: no dependency information about nuclos"
	exit -2
fi

EXTENSION_DIR=`pwd`
APP_LIST_DIR=`dirname $0`
APP_LIST_DIR=`readlink -e $APP_LIST_DIR`
REGEX_IGNORE="(aspectjtools|aspectjrt|nuclos-common-api|nuclos-client-api)-.*jar"

copyDeps() {
	local DIR="$1"
	shift
	local COPY_DIR="$1"
	shift
	pushd "$DIR"
		rm -f "list.txt"
		# https://maven.apache.org/plugins/maven-dependency-plugin/list-mojo.html
		mvn dependency:list -DincludeScope=runtime -DoutputFile=list.txt >>/dev/null
	popd
	# http://stackoverflow.com/questions/4780203/deleting-lines-from-one-file-which-are-in-another-file
	rm -f result.txt
	grep -x -v -i -f "$EXTENSION_DIR/nuclos.txt" "$DIR/list.txt" >result.txt
	for i in `cat result.txt`; do
		if [[ "$i" =~ $REGEX_DEP ]]; then
			echo "$i"
			local GROUPID="${BASH_REMATCH[1]}"
			local ARTEFACTID="${BASH_REMATCH[2]}"
			local TYPE="${BASH_REMATCH[3]}"
			local VERSION="${BASH_REMATCH[4]}"
			local SCOPE="${BASH_REMATCH[5]}"
			local PATH=`echo "$GROUPID" | /bin/sed -e 's/\./\//g'`
			local FILE_DEP="$MAVEN_REPOSITORY/$PATH/$ARTEFACTID/$VERSION/$ARTEFACTID-$VERSION.jar"
			# echo "$GROUPID -> $PATH"
			# echo "$FILE_DEP"
			# ls "$FILE_DEP"
			/bin/cp "$FILE_DEP" "$COPY_DIR"
		fi
	done
	/bin/rm -f "$DIR/list.txt"
}

processDeps() {
	local DIR="$1"
	shift
	local COPY_DIR="$1"
	shift
	local TARGET_DIR="$1"
	shift
	copyDeps "$DIR" "$COPY_DIR"
	pushd "$COPY_DIR"
		for i in *.jar; do
			if [[ "$i" =~ $REGEX_IGNORE ]]; then
				continue;
			fi
			unsign "$i"
			sign "$i"
			echo "cp $i $TARGET_DIR"
			cp "$i" "$TARGET_DIR"
		done
	popd
}

unsign() {
        local JAR="$1"
        shift
        zip -q -d "$JAR" META-INF/\*.SF META-INF/\*.DSA META-INF/\*.RSA META-INF/INDEX.LIST >>/dev/null
        # META-INF/MANIFEST.MF
}

sign() {
        local JAR="$1"
        shift
        jarsigner -keystore "$KEYSTORE_FILE" -storepass "$KEYSTORE_STOREPASSWD" \
		-keypass "$KEYSTORE_KEYPASSWD" "$JAR" "$KEYSTORE_KEYALIAS" >>/dev/null
}

KEYSTORE_FILE="$EXTENSION_DIR/key"
# Check for 'official' key store
if [[ -n "$KEYSTORE" ]]; then
	KEYSTORE_KEYALIAS="$ALIAS"
	KEYSTORE_STOREPASSWD="$STOREPASS"
	KEYSTORE_KEYPASSWD="$KEYPASS"
	cp "$KEYSTORE" "$KEYSTORE_FILE"
fi
# Create keystore if needed
if [ ! -f "$KEYSTORE_FILE" ]; then
	keytool -genkeypair -dname "$KEYSTORE_DNAME" \
		-alias "$KEYSTORE_KEYALIAS" -keypass "$KEYSTORE_KEYPASSWD" \
		-keystore "$KEYSTORE_FILE" -storepass "$KEYSTORE_STOREPASSWD" \
		-validity 180 || exit -2
fi

# http://wiki.nuclos.de/display/Konfiguration/Entwicklungsumgebung
pushd "$INSTALL_DIR"
	rm -rf "extensions"
	mkdir -p "extensions/common/native"
	mkdir -p "extensions/client/themes"
	mkdir -p "extensions/server"
popd

COMMON_DIR=`ls -d *-common`
CLIENT_DIR=`ls -d *-client`
SERVER_DIR=`ls -d *-server`
if [ -n "$COMMON_DIR" ]; then
	pushd "$COMMON_DIR/target"
		JAR=`ls *-common-*.jar`
		unsign "$JAR"
		sign "$JAR"
	popd
	cp *-common/target/*-common-*.jar "$INSTALL_DIR/extensions/common"
	rm -rf deps
	mkdir deps
	processDeps "$COMMON_DIR" deps "$INSTALL_DIR/extensions/common"
fi
if [ -n "$CLIENT_DIR" ]; then
	pushd "$CLIENT_DIR/target"
		JAR=`ls *-client-*.jar`
		unsign "$JAR"
		sign "$JAR"
	popd
	cp *-client/target/*-client-*.jar "$INSTALL_DIR/extensions/client"
	rm -rf deps
	mkdir deps
	processDeps $CLIENT_DIR deps "$INSTALL_DIR/extensions/client"
	rm -f $INSTALL_DIR/extensions/client/*-common-*.jar
fi

cp *-theme*/target/*-theme*-*.jar "$INSTALL_DIR/extensions/client/themes"
cp *-server/target/*-server-*.jar "$INSTALL_DIR/extensions/server"

rm -rf deps list.txt result.txt

tree "$INSTALL_DIR/extensions"
